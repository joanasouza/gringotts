# README

Essa é uma aplicação de estudo para aprender e entender rails. Esse exercício tem o objetivo de simular uma mini aplicação bancária, a qual, permite um cliente consultar saldo e fazer transferências.

## Gringotts Wizarding Bank

O nome utilizado para o projeto, é uma referência ao sistema bancário do universo Harry Potter. Apesar da escolha do nome, não pretendo replicar algo pensando no universo de HP, e sim simular ações de um banco do mundo real.

P.S: gosto de Harry Potter, porém não compartilho de várias opiniões da autora em diversas questões.

## Especificações do projeto

* Ruby version
    2.7.1

* Database creation

* Database initialization

* How to run the test suite


## Funcionalidades

- [ ] Verificar saldo de conta
- [ ] Fazer transferências
- [ ] Checar histórico de conta

## Endpoints

- /login  - GET
- /login - POST

- /balance - GET

- /transfer - GET
- /transfer - POST

- /history - GET

